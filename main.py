from flask import Flask, render_template, Response, json, request,jsonify
from config import Config
import model
import json

app = Flask(__name__)
app.config.from_object(Config)
app.config["DEBUG"] = True


@app.route('/_autocomplete', methods=['get'])
def autocomplete():
    cities = model.listartists
    return Response(json.dumps(cities), mimetype='application/json')


@app.route('/')
def register():
    return '127.0.0.1:5000/prediction?artist=Eminem&song=Darkness&nb=4'

@app.route('/prediction', methods=['GET'])
def get_prediction():
    feature1 = str(request.args.get('artist'))
    feature2 = str(request.args.get('song'))
    feature3 = int(request.args.get('nb'))
    feature4 = "['" + feature1 + "']"
    listeA, listeB = model.musicSelections(feature2, feature3, feature4)
    dictionnaire = {x: y for x, y in zip(listeA, listeB)}
    resp = jsonify(dictionnaire)
    resp.status_code = 200
    return (resp)