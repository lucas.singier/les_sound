1) Ouvrir le fichier main.py dans un notebook et enlever les guillemets dans le lien débutant par 127.0.0.1:5000/... à "Eminem" et "Darkness".

2) Ouvrir un nouveau projet avec PyCharm et choisir le projet les_sound importé

3) Installer les imports requis dans le terminal de PyCharm, à savoir:

pip install flask
pip install flask-WTF
pip install sklearn
pip install pandas

4) Ensuite, définir les variables d'environnement:
set FLASK_ENV=development
set FLASK_APP=main

5) Lancer flask -> flask run

6) Aller sur localhost:5000
Copier-coller le lien affcihé sur ce port et y accéder

7) Patienter et les prédictions vont s'afficher sous format JSON