# -*- coding: utf-8 -*-
"""
Created on Sat Jan 16 13:12:23 2021

@author: Admin
"""
# =============================================================================
# Import
# =============================================================================
import pandas as pd
#import plotly
from sklearn.preprocessing import StandardScaler
from scipy.spatial import distance

# =============================================================================
# Read CSV
# =============================================================================
df=pd.read_csv('data/data.csv')

df.drop(columns=['id','release_date'],inplace=True)

# =============================================================================
# Drop Features
# =============================================================================
indexNames = df[ df['artists'] == "['Эрнест Хемингуэй']" ].index
df.drop(indexNames , inplace=True)
indexNames = df[ df['artists'] == "['Эрих Мария Ремарк']" ].index
df.drop(indexNames , inplace=True)
indexNames = df[ df['artists'] == "['Unknown']" ].index
df.drop(indexNames , inplace=True)

# =============================================================================
# Scaler
# =============================================================================
#Prend toutes les colonnes sauf artist et name
x=df[df.drop(columns=['artists','name']).columns].values
scaler =StandardScaler().fit(x)
X_scaled = scaler.transform(x)
#Scale sur les colonnes sans artist et name
df[df.drop(columns=['artists','name']).columns]=X_scaled
# df.to_csv('data/datav2.csv',index=False)

# =============================================================================
# Model
# =============================================================================
def musicSelections(name,nbmusic,artist):
    data=df
    #Drop les doublons avec nom et artiste en sous ensemble
    data.drop_duplicates(subset=['artists','name'],inplace=True)
    
    #Creation d'un dataframe avec les valeurs de la musique sans le nom et l'artiste
    dataframe_sans_artiste_nom=data[(data['name']==name) & (data['artists']==artist)].drop(columns=['name','artists']).values
    
    # On enlève les '' et les [] pour l'artiste
    
    artist=artist.replace("'","").replace('[','').replace(']','')
    #replace ['
    #On print la musique choisie
    print('Musique écoutée :',name,' par ',artist)
    
    list_musiques = data['name'].values
    dfsansartistname = data.drop(columns=['artists','name']).values
    p=[]
    count=0
    # Parcours le dataframe des noms et on met dans la liste la correlation entre la ligne et les autres lignes.
    #le count permet de passé à une autre musique
    for i in dfsansartistname:
        p.append([distance.correlation(dataframe_sans_artiste_nom,i),count])
        count+=1
    p.sort()
    listeArtiste=[]
    listemusique=[]
    for i in range(1,nbmusic+1):
        artists=df['artists'].values
        artist=artists[p[i][1]]
        artist=artist.replace("'","").replace('[','').replace(']','')
        # Print la liste des musiques
        #print(list_musiques[p[i][1]],' : ',artist)
        listeArtiste.append(list_musiques[p[i][1]])
        listemusique.append(artist)
    return(listeArtiste,listemusique)

#a,b = musicSelections('Smells Like Nirvana',5,'[\'"Weird Al" Yankovic\']')
#print(a)
# =============================================================================
# Liste Input Artist
# =============================================================================
listartist=df['artists']#.tolist()
# (168373,)
listartist = listartist.unique()
listartist = listartist.tolist()
#print(listartist)
listartists = []
for string in listartist:
    new_string = string.replace("['", "").replace("']", "").replace("\"", "")
    listartists.append(new_string)
# print(listartists)

# =============================================================================
# Liste Input 
# =============================================================================

listarmu = df[['artists','name']]
listes = listarmu[listarmu['artists'] == "['Eminem']"]
listes = listes['name'].tolist()
#print(listes)

def artistforme(artist):
    return  "['"+artist+"']"

def findlistmusic(artist):
    listedf = listarmu
    listes = listedf[listedf['artists'] == artist]
    listes = listes['name'].replace("'","").tolist()
    return listes